//
//  flashVC.swift
//  EmotionAll
//
//  Created by Penafiel, Vanessa on 7/25/18.
//  Copyright © 2018 Penafiel, Vanessa. All rights reserved.
//

import UIKit
import AVFoundation

class flashVC: UIViewController{

var images: Array = [UIImage]()
var imageSounds: Array = [String]()
var imageNames: Array = [String]()
var count: Int = 0
var audioPlayer = AVAudioPlayer()

required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    getAllImages()
}
    
    @IBOutlet weak var btnImgView: UIImageView! {
        didSet {
            btnImgView.image = images[count]
            btnImgView.translatesAutoresizingMaskIntoConstraints=true
        }
    }
    
    @IBOutlet weak var btnNext: UIButton!{
        didSet{
            btnNext.setTitle("Next >", for: .normal)
            btnNext.setTitleColor(UIColor.black, for: .normal)
            btnNext.backgroundColor=UIColor.yellow
            btnNext.translatesAutoresizingMaskIntoConstraints=true
        }
    }
    
    @IBOutlet weak var btnPrev: UIButton!{
        didSet{
            btnPrev.setTitle("< Previous", for: .normal)
            btnPrev.setTitleColor(UIColor.black, for:.normal)
            btnPrev.backgroundColor=UIColor.yellow
            btnPrev.translatesAutoresizingMaskIntoConstraints=true
        }
    }
    
    @IBOutlet weak var imgBtn: UIButton!
    
    @IBAction func userDidTapBtnPrev(_ sender: Any) {
        count = count - 1
        if (count < 0 ) {
            count = images.count - 1
        }
        
        btnImgView.image = images[count]
        imgBtn.setTitle("", for: UIControlState.normal)
    }
    @IBAction func userDidTapBtnNext(_ sender: Any) {
        count = count + 1
        if (count >= images.count ) {
            count = 0
        }
        btnImgView.image = images[count]
        imgBtn.setTitle("", for: UIControlState.normal)
    }
    
    @IBAction func flipCard(_ sender: UIButton){
        let button = sender
        let title = button.title(for: UIControlState.normal)
        if title == "" {
            button.setTitle(imageNames[count], for: UIControlState.normal)
            button.titleLabel?.textColor = UIColor.white
            button.titleEdgeInsets = UIEdgeInsetsMake(0.0, -5.0, 5.0, 0.0)
//            let music = Bundle.main.path(forResource: "happy", ofType: "wav")
            // tells the compiler what to do when action is received
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: imageSounds[count] ))
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
                try AVAudioSession.sharedInstance().setActive(true)
                audioPlayer.play()
            }
            catch{
                print(error)
            }
            button.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        }else{
            button.setTitle("", for: UIControlState.normal)
            button.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        }
    }
    
    func getAllImages(){
        for i in 1...6{
            let name: String = "img"+String(i)
            let image = UIImage.init(named: name)

            images.append(image!)
        }
        imageNames = ["Happy", "Angry", "Sad", "Scared", "Surprised", "Confused"]
        for i in 0...5{
            let sound = Bundle.main.path(forResource: imageNames[i], ofType: "wav")
            imageSounds.append(sound!)
        }
    }
    @IBAction func play(_ sender: AnyObject) {
        audioPlayer.play()
}
}
